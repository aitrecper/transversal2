# README #

Projecte transversal 2.

### Integrants del projecte: ###

* Aitor Recio Pérez
* Larisa Petrea
* Kevin Sequera Ríos

### Objectiu breu ###

El nostre objedtiu és crear una página web per fer enquestes.

### Estat del projecte ###

* Plantejament

### Tasques realitzades: ###

* [Full de càlcul a Google Drive](https://drive.google.com/open?id=1EBM4z5u7zhiuGQoyYmFQanONGMMH4jf8JprwDqM32qM)

### Adreça web de la documentació ###

* [Enllaç al labs]

### Adreça web del projecte desplegat ###

* [Enllaç al labs]