<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Entity\Category;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EnquestesController extends Controller
{

    /**
     * @Route("/enquestes", name="enquestes")
     */
    public function selectAllEnquestes()
    {
        $enquestes = $this->getDoctrine()
            ->getRepository('AppBundle:Enquesta')
            ->findAll();

        /*if (count($products)==0) {
            return $this->render('default/message.html.twig', array(
                'message' => 'No products found'));
        }*/
        return $this->render('enquestes/enquestes.html.twig', array(
            'enquesta' => $enquestes));
    }
}
