<?php
 namespace AppBundle\Form;
 use Doctrine\DBAL\Types\DateTimeType;
 use Symfony\Component\Form\AbstractType;
 use Symfony\Component\Form\Extension\Core\Type\DateType;
 use Symfony\Component\Form\Extension\Core\Type\TimeType;
 use Symfony\Component\Form\FormBuilderInterface;
 use Symfony\Component\OptionsResolver\OptionsResolver;

 class EnquestaType extends AbstractType

 {

     /**

      * {@inheritdoc}

      */

     public function buildForm(FormBuilderInterface $builder, array $options)

     {

         $builder->add('pregunta')

                 ->add('dataInici', DateType::class, array(


                    

                    'years' => range(date('Y'), date('Y')+5),

                    'widget' => 'choice',


                    'years' => range(date('Y'), date('Y')+5), 
                    'format' => 'dd-M-yyyy',

                    'attr' => array(

                        'class' => 'dataI'),

                    'data' => new \DateTime("tomorrow"), 

                 ))

                 ->add('dataFinal', DateType::class, array(

                    

                   

                    'widget' => 'choice',

                    

                     'years' => range(date('Y'), date('Y')+5),

                    'format' => 'dd-M-yyyy',

                   'attr' => array(

                        'class' => 'dataF'),

                    'data' => new \DateTime("tomorrow + 1day"), 

                 ))

                 ->add('destacada');

     }/**

      * {@inheritdoc}

      */

 

     public function configureOptions(OptionsResolver $resolver)

     {

         $resolver->setDefaults(array(

             'data_class' => 'AppBundle\Entity\Enquesta'

         ));

     }

 

     /**

      * {@inheritdoc}

      */

     public function getBlockPrefix()

     {

         return 'appbundle_enquesta';

     }

 

 

 }
