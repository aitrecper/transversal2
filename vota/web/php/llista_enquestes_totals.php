<?php
require ('datos_conn.php');

$conn = mysqli_connect($serv, $username, $passw, $db);

if(!$conn){
  die('Error de Conexion: '. mysqli_connect_errno());
}

class Enquesta{
  public $id;
  public $pregunta;
  public $data_inici;
  public $data_final;
  public $destacada;
  public $si;
  public $no;
}

$value = [1, 0];

$query = "SELECT * FROM enquesta";
$resultado = mysqli_query($conn, $query);

$preg = array(
  'data' => array()
);

if(!$resultado){
  die("Error");
}
else{

  while($data = mysqli_fetch_object($resultado, $class_name = "Enquesta")) {
    if($stmt = mysqli_prepare($conn, "SELECT count(*) AS si FROM enquesta INNER JOIN resposta WHERE id_enquesta = ? AND enquesta.id = resposta.id_enquesta AND valor = ? GROUP BY id_enquesta")){
      mysqli_stmt_bind_param($stmt, "ii", $data->id, $value[0]);
      mysqli_stmt_execute($stmt);
      mysqli_stmt_bind_result($stmt, $numSi);
      mysqli_stmt_fetch($stmt);
      $data->si =  $numSi;

      mysqli_stmt_bind_param($stmt, "ii", $data->id, $value[1]);
      mysqli_stmt_execute($stmt);
      mysqli_stmt_bind_result($stmt, $numNo);
      mysqli_stmt_fetch($stmt);
      $data->no =  $numNo;

      mysqli_stmt_close($stmt);

      array_push($preg['data'], $data);

    }else{
      $preg = "Error";
    }

    }
}

print json_encode($preg);

mysqli_free_result($resultado);
mysqli_close($conn);


?>
