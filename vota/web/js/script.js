//Mostra les enquestes al carregar la pàgina------------------------------------------------------------------------

$(function () {
    $('#tornar').hide();

    var nrespuestas=0;
    $.ajax({
        type: 'post',
		    url:'../php/llista_enquestes.php',
        dataType: 'JSON'
		}).done(function(res){

      console.log(res);
      $('#login-stuff').html(`
        <form id="formulari-usr" class="form-inline" action="">
      			<input type="text" name="usuari" placeholder="Usuari">
            <br>
      			<input type="password" name="password" placeholder="Contrassenya">
            <br>
      			<input type="submit" id="boton-log" class="btn btn-primary" value=Enviar>
      		</form>`);
      let taula=`<table width=100% id="taula"><tr><th>Pregunta</th><th>Data Inici</th><th>Data final</th><th>Grafic</th></tr>`;
      $('.res').html('<h2><font color="#007bff">Enquestes destacades</font></h2>');
      $('.res').append();

			$.each(res.data, function(index, info){

                $('.res').append(`
                  <div class="pregunta">${info.pregunta}</div>
                  <div class="data_inici">${info.data_inici}</div>
                  <div class="data-final">${info.data_final}</div>
                  <div class="canvas-cont"><canvas id="${info.id}" width="20px"></div>
                `);

                if(info.si == undefined && info.no == undefined) {
                  mostrarGrafico(info.id, 0, 0);
                }
				        else if(info.si == undefined){
                  mostrarGrafico(info.id, 0, info.no);
                }
				        else if(info.no == undefined){
                  mostrarGrafico(info.id, info.si, 0);
                }
				        else {
                  mostrarGrafico(info.id, info.si, info.no);
               }
      });
      $('.btn-secondary').hide()
     });
   });




/*Mostra el formulari, comproba si es vaid l'usuari o si es admin. Si es admin redirigeix a la pagina de admin, si es un
usuari normal li dona la benvinguda y oculta el formulari*/
//----------------------------------------------------------------------------------------------------------------------

$('body').on('click', '#boton-log',function(e){

  e.preventDefault();
    $.ajax({
     type: 'post',
     url: '../php/login_usuari.php',
     data: $('#formulari-usr').serialize(),
     dataType: 'JSON'
   }).done(function(res){
        console.log(res);
        if(res){
          $('.saludo').css('color','#007bff');
          $.each(res.usuarios, function(index, info){
            $('#login-stuff').hide();
            $('.saludo').show();
            $('.saludo').html("");
            $('.saludo').html('Benvingut/a <span class="usuario">'+`${info.username}`+'</span>'+'<br>'+'<a href="" class="cerrar_sesion">Tancar sessió</a>');
            if (info.admin==1) {
              window.location.href = '/admin';
            }
          });
          $('.formulari').hide();
          $('.res').html();
          var usr = $('.usuario').text();
        }
        else{
          $('.saludo').css('color','red');
          $('.saludo').text('Usuari no valid');
        }
    })
});

$("body").on("click", ".pregunta", function(){

    var saludo = $('.saludo').text();
    console.log(saludo);
    $('#grafico-grande').hide();

    if(saludo=="" || saludo == "Has d'estar enregistrat per poder votar en una enquesta"){
      $('.saludo').css('color','red');
      $('.saludo').text("");
      $('.saludo').text("Has d'estar enregistrat per poder votar en una enquesta");
    }
    else{

      var valor=$(this).text();

      $('.res').hide();
      $('.formulari-enquesta').show();
      $('.formulari-enquesta').html(`
          <h3 name="titol-preg" class="titol-enquesta"></h3>
      		<form class="form2" action="">
      				<input type="radio" name="vot" value=1>Si
      			 	<input type="radio" name="vot" value=2>No<br>
      			<br>
      			<input type="button" class="btn btn-primary" id="votar"value=Votar>
      		</form>
          `);
      $('.btn-secondary').show();
      $('.titol-enquesta').text(valor);
    }

});

$("body").on('click','#votar',function(e){

    var valor = $('.formulari-enquesta > h3').text();
    var saludo = $('.usuario').text();
    e.preventDefault();
    $.ajax({
       type: 'post',
       url: '../php/votacio_enquesta.php',
       data:$('.form2').serialize()+"&valor="+valor+"&saludo="+saludo,
       dataType: 'JSON'
    }).done(function(res){
        $('.mensaje').show();
          console.log(res);
          if(res==true){
              $.ajax({
                  type: 'post',
          		    url:'../php/llista_enquestes.php',
                  dataType: 'JSON'
          		}).done(function(res){
                var id_enquesta = res.data.id;
                console.log(res.data[0].id);
                $('.formulari-enquesta').hide();
                $('.mensaje').show();
                $('#grafico-grande').show();
                $('.mensaje').text('El seu vot ha sigut recollit satisfactoriament');
                $('.mensaje').css('color','green');
                var id= $(this).click()

                $('#grafico-grande').append(`
                    <canvas id="${res.data.id}" width="20px">
                `);
                    mostrarGrafico(res.data.id, 1,3);
                });
        }

        if(res==false){
            $('.mensaje').css('color','red');
            $('.mensaje').text("No pot votar dues vegades la mateixa enquesta");
        }
        if(res==null){
            $('.mensaje').show();
            $('.mensaje').css('color','red');
            $('.mensaje').text("No es pot votar en blanc");
        }
      });
  });

$('body').on('click', '#tornar', function(){
  $('.res').show();
  $('#tornar').hide();
  $('.mensaje').hide();
  $('.formulari-enquesta').hide();
  $('#grafico-grande').hide();
  $('.res').append();
  $.ajax({
      type: 'post',
      url:'../php/llista_enquestes.php',
      dataType: 'JSON'
  }).done(function(res){

    console.log(res);
    $('.res').html(" ");
    $.each(res.data, function(index, info){
              $('.res').append(`
                <div class="pregunta">${info.pregunta}</div>
                <div class="data_inici">${info.data_inici}</div>
                <div class="data-final">${info.data_final}</div>
                <div class="canvas-cont"><canvas id="${info.id}" width="20px"></div>
              `);

              if(info.si == undefined && info.no == undefined) {
                mostrarGrafico(info.id, 0, 0);
              }
              else if(info.si == undefined){
                mostrarGrafico(info.id, 0, info.no);
              }
              else if(info.no == undefined){
                mostrarGrafico(info.id, info.si, 0);
              }
              else {
                mostrarGrafico(info.id, info.si, info.no);
             }
    });

  });

})

$('body').on('click', '.cerrar_sesion', function(){


})

// En construccio
//-------------------------------------------------------------------
