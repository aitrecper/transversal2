$(document).ready(function() {
  $("#add_err").css("display", "none", "important");
  $("#login").click(function() {
    username = $("#user").val();
    password = $("#pass").val();
    codigo = "user=" + username + "&pass=" + password;
    console.log(username + " " + password + " " + codigo);
    $.ajax({
      type: "GET",
      url: "login.php",
      data: codigo,
      success: function(html) {
        if (html === "true") {
          //$("#add_err").html("right username or password");
          $("#add_err").css("display", "inline", "important");
          $("#add_err").html("Bienvenido");
        } else if (html === "true admin") {
          window.location = "enquestes.php";
        } else {
          $("#add_err").css("display", "inline", "important");
          $("#add_err").html("Wrong username or password");
        }
      },
      beforeSend: function() {
        $("#add_err").css("display", "inline", "important");
        $("#add_err").html("Loading...");
      }
    });
    return false;
  });
  google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Sí o No?', 'Quantitat'],
          ['Sí',1],
          ['No',1]
        ]);

        var options = {
          title: 'Sí o No?'
        };

        var chart = new google.visualization.PieChart(document.getElementById('destacadas'));

        chart.draw(data, options);
      }

//   $.ajax({
//     type:"GET",
//     url:""
//   });
});
