<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Vota!</title>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>  
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript" src="script.js"></script>
</head>

<body>
  <h1>
    Vota!
  </h1>
  <form action="./" method="post">
    <div class="err" id="add_err"></div>
    <input type="text" name="user" placeholder="Usuari" id="user">
    <input type="password" name="pass" placeholder="Contrasenya" id="pass">
    <input type="submit" name="submit" value="Login" id="login">
  </form>
  <div id="destacadas" style="background-color:#c3c3c3"></div>
</body>

</html>