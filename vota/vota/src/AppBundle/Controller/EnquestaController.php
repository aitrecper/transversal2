<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Enquesta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Enquestum controller.
 *
 */
class EnquestaController extends Controller
{
    /**
     * Lists all enquestum entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $enquestas = $em->getRepository('AppBundle:Enquesta')->findAll();

        return $this->render('enquesta/index.html.twig', array(
            'enquestas' => $enquestas,
        ));
    }

    /**
     * Creates a new enquestum entity.
     *
     */
    public function newAction(Request $request)
    {
        $enquestum = new Enquesta();
        $form = $this->createForm('AppBundle\Form\EnquestaType', $enquestum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($enquestum);
            $em->flush();

            return $this->redirectToRoute('enquesta_show', array('id' => $enquestum->getId()));
        }

        return $this->render('enquesta/new.html.twig', array(
            'enquestum' => $enquestum,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a enquestum entity.
     *
     */
    public function showAction(Enquesta $enquestum)
    {
        $deleteForm = $this->createDeleteForm($enquestum);

        return $this->render('enquesta/show.html.twig', array(
            'enquestum' => $enquestum,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing enquestum entity.
     *
     */
    public function editAction(Request $request, Enquesta $enquestum)
    {
        $deleteForm = $this->createDeleteForm($enquestum);
        $editForm = $this->createForm('AppBundle\Form\EnquestaType', $enquestum);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('enquesta_edit', array('id' => $enquestum->getId()));
        }

        return $this->render('enquesta/edit.html.twig', array(
            'enquestum' => $enquestum,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a enquestum entity.
     *
     */
    public function deleteAction(Request $request, Enquesta $enquestum)
    {
        $form = $this->createDeleteForm($enquestum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($enquestum);
            $em->flush();
        }

        return $this->redirectToRoute('enquesta_index');
    }

    /**
     * Creates a form to delete a enquestum entity.
     *
     * @param Enquesta $enquestum The enquestum entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Enquesta $enquestum)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('enquesta_delete', array('id' => $enquestum->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
